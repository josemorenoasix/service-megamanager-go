package repositories

import (
	"fmt"
	"service-megamanager-go/interfaces"
	"service-megamanager-go/models"
	"github.com/afex/hystrix-go/hystrix"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

type AccountRepositoryWithCircuitBreaker struct {
	AccountRepository interfaces.IAccountRepository
}

func (repository *AccountRepositoryWithCircuitBreaker) GetAccountById(id int) (models.AccountModel, error) {

	output := make(chan models.AccountModel, 1)
	hystrix.ConfigureCommand("get_account_by_id", hystrix.CommandConfig{Timeout: 1000})
	errors := hystrix.Go("get_account_by_id", func() error {

		account, _ := repository.AccountRepository.GetAccountById(id)

		output <- account
		return nil
	}, nil)

	select {
	case out := <-output:
		return out, nil
	case err := <-errors:
		println(err)
		return models.AccountModel{}, err
	}
}

type AccountRepository struct {
	interfaces.IDbHandler
}

func (repository *AccountRepository) GetAccountById(id int) (models.AccountModel, error) {

	row, err := repository.Query(fmt.Sprintf("SELECT * FROM account_models WHERE id = '%d'", id))
	if err != nil {
		return models.AccountModel{}, err
	}

	var account models.AccountModel

	row.Next()
	row.Scan(
		&account.Id,
		&account.Email,
		&account.Password,
		&account.Registered,
		&account.FuseMount,
		&account.FolderName)

	return account, nil
}
