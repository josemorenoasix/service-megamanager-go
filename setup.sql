CREATE TABLE account_models (id INT, email TEXT, password TEXT, registered INT, fusemount INT, folder TEXT);

INSERT INTO account_models (id, email, password, registered, fusemount, folder) VALUES (101, "email+s01@domain.test", "topSecret", 0, 0, "[FOLDER] MyFolder to Sync - 01");
INSERT INTO account_models (id, email, password, registered, fusemount, folder) VALUES (102, "email+s02@domain.test", "topSecret", 0, 0, "[FOLDER] MyFolder to Sync - 02");
INSERT INTO account_models (id, email, password, registered, fusemount, folder) VALUES (103, "email+s03@domain.test", "topSecret", 0, 0, "[FOLDER] MyFolder to Sync - 03");
INSERT INTO account_models (id, email, password, registered, fusemount, folder) VALUES (104, "email+s04@domain.test", "topSecret", 0, 0, "[FOLDER] MyFolder to Sync - 04");
