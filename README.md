# service-megamanager-go
-------
This is a project of a Web Service written with Go for managing multiple MEGA accounts

Used dependencies:
 - [Gorm (ORM library)](https://github.com/jinzhu/gorm)
 - [Hystrix-Go (Circuit Breaker)](https://github.com/afex/hystrix-go)

Get Started:

 - [Install](https://gitlab.com/josemorenoasix/service-megamanager-go/#install)
 - [Introduction](https://gitlab.com/josemorenoasix/service-megamanager-go/#introduction)

----------

[Install](https://gitlab.com/josemorenoasix/service-megamanager-go/#install)
-------

Clone the source

    git clone https://gitlab.com/josemorenoasix/service-megamanager-go.git

Setup dependencies

    go get -u github.com/go-chi/chi
    go get -u github.com/jinzhu/gorm
    go get github.com/afex/hystrix-go/hystrix

Setup sqlite data structure

    sqlite3 /var/tmp/megamanager.sqlite3 < setup.sql

----------

[Introduction](https://gitlab.com/josemorenoasix/service-megamanager-go/#introduction)
-------
This is a project of a Web Service written with Go for managing multiple MEGA accounts
