package models

type AccountModel struct {
	Id			int
	Email		string
	Password	string
	Registered	bool
	FuseMount	bool
	FolderName	string
}
