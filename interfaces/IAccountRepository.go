package interfaces

import (
	"service-megamanager-go/models"
)

type IAccountRepository interface {
	GetAccountById(id int) (models.AccountModel, error)
}
