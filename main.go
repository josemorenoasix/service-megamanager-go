package main

import (
	"database/sql"
	"service-megamanager-go/infrastructures"
	"service-megamanager-go/repositories"
	"encoding/json"
	"fmt"
)

func main() {

	sqlConn, _ := sql.Open("sqlite3", "/var/tmp/megamanager.sqlite")
	sqliteHandler := &infrastructures.SQLiteHandler{}
	sqliteHandler.Conn = sqlConn

	accountRepository := &repositories.AccountRepository{sqliteHandler}
	account, _ := accountRepository.GetAccountById(101)

	accountJson, _ := json.MarshalIndent(account, "", "  ")
	fmt.Println(string(accountJson))
}
